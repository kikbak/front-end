// Actions
const SET_CREATOR = 'creators/SET_CREATOR';
const CLEAR_CREATOR = 'creators/CLEAR_CREATOR';

// Reducer
export default function reducer(state = null, action = {}) {
  switch (action.type) {
    case SET_CREATOR:
      return action.creator;
    case CLEAR_CREATOR:
      return null;

    default:
      return state;
  }
}

// Action Creators
export function setCreator(creator) {
  if (creator.token) {
    window.localStorage.setItem('token', creator.token);
  }
  return { type: SET_CREATOR, creator };
}

export function clearCreator() {
  window.localStorage.removeItem('token');
  return { type: CLEAR_CREATOR };
}
