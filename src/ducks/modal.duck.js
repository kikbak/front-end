const SHOW_MODAL = '/modals.SET_MODAL';
const CLOSE_MODAL = '/modals.CLOSE_MODAL';

const initialState = {
  modalType: null,
  modalProps: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        modalProps: action.modalProps,
        modalType: action.modalType,
        type: action.type
      };
    case CLOSE_MODAL:
      return initialState;
    default:
      return state;
  }
}

export const showModal = ({ modalProps, modalType }) => {
  return {
    type: SHOW_MODAL,
    modalProps,
    modalType
  };
};

export const closeModal = () => {
  return {
    type: CLOSE_MODAL
  };
};
