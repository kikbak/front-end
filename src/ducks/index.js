import { createStore, combineReducers } from 'redux';

import creator from './creator.duck';
import modal from './modal.duck';

const rootReducer = combineReducers({ creator, modal });

const store = createStore(rootReducer);

export default store;
