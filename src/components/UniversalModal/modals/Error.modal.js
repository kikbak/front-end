import React from 'react';
import {
  DialogTitle,
  DialogActions,
  Button,
  DialogContentText
} from '@material-ui/core';

class ErrorModal extends React.Component {
  render() {
    const { onClose } = this.props;

    return (
      <React.Fragment>
        <DialogTitle>Hmm....</DialogTitle>
        <DialogContentText style={{ textAlign: 'center' }}>
          It looks like we haven't built this out yet, we've got it under
          control though.
        </DialogContentText>
        <DialogActions>
          <Button onClick={onClose} variant="contained" color="primary">
            Close
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  }
}

export default ErrorModal;
