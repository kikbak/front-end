import React from 'react';
import http from 'helpers/http.helper';
import { Toast } from 'components';
import {
  DialogTitle,
  DialogActions,
  Button,
  DialogContent,
  TextField
} from '@material-ui/core';
import styled from 'styled-components';

const PreviewPhoto = styled.img`
  width: 50%;
  margin: 0 auto;
`;

class PortfolioUploadModal extends React.Component {
  state = {
    caption: '',
    file: null,
    previewPhoto: ''
  };

  onChange = e => {
    this.setState({
      file: e.target.files[0],
      previewPhoto: URL.createObjectURL(e.target.files[0])
    });
  };

  save = e => {
    e.preventDefault();

    const file = this.state.file;
    const form = new FormData();

    form.append('file', file);

    http()
      .post('/img-uploads', form)
      .then(res => {
        http().post(`/portfolio-posts`, {
          imgUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/' + res.fileName,
          caption: this.state.caption
        });

        this.props.onClose();
      })
      .catch(err => Toast.show(err.message));
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    const { onClose } = this.props;
    const { file } = this.state;

    return (
      <React.Fragment>
        <DialogTitle style={{ textAlign: 'center' }}>
          Upload Content
        </DialogTitle>
        <DialogContent style={{ textAlign: 'center' }}>
          <form style={{ padding: 25 }} onSubmit={this.save}>
            {file ? (
              <div style={{ position: 'relative' }}>
                <PreviewPhoto src={this.state.previewPhoto} />
              </div>
            ) : (
              <React.Fragment>
                <input
                  accept="image/*"
                  id="bt-img-upload"
                  style={{ display: 'none' }}
                  type="file"
                  onChange={this.onChange}
                />
                <label htmlFor="bt-img-upload">
                  <Button
                    variant="text"
                    color="primary"
                    fullWidth
                    component="span"
                  >
                    Upload from computer
                  </Button>
                </label>
              </React.Fragment>
            )}

            <TextField
              name="caption"
              label="caption"
              autoFocus
              required
              fullWidth
              margin="normal"
              value={this.state.caption}
              onChange={this.handleChange}
            />

            <DialogActions>
              <Button onClick={onClose} variant="contained" color="secondary">
                Close
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </React.Fragment>
    );
  }
}

export default PortfolioUploadModal;
