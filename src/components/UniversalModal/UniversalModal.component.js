import React from 'react';
import { connect } from 'react-redux';
import { closeModal } from 'ducks/modal.duck';
import { Dialog } from '@material-ui/core';
import ErrorModal from './modals/Error.modal';
import PortfolioUploadModal from './modals/PortfolioUpload.modal';

class UniversalModal extends React.Component {
  _renderInnerModal = type => {
    switch (type) {
      case 'portfolioUpload':
        return <PortfolioUploadModal onClose={this.props.closeModal} />;
      default:
        return <ErrorModal onClose={this.props.closeModal} />;
    }
  };

  render() {
    return (
      <Dialog
        fullWidth
        maxWidth="sm"
        open={Boolean(this.props.modalType)}
        onClose={this.props.closeModal}
        refresh={this.props.refresh}
      >
        {this._renderInnerModal(this.props.modalType)}
      </Dialog>
    );
  }
}

export default connect(
  state => ({
    modalType: state.modal.modalType,
    modalProps: state.modal.modalProps
  }),
  { closeModal: closeModal }
)(UniversalModal);
