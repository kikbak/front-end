import React from 'react';
import { Toast } from 'components';
import { setCreator } from 'ducks/creator.duck';
import http from 'helpers/http.helper.js';
import { connect } from 'react-redux';
import queryString from 'query-string';

class AuthWrapper extends React.Component {
  state = {
    loading: true
  };

  componentDidMount() {
    const urlToken = queryString.parse(window.location.search).token;

    if (urlToken) {
      window.localStorage.setItem('token', urlToken);
    }

    if (window.localStorage.getItem('token')) {
      http()
        .get('/creators/me')
        .then(creator => {
          this.props.setCreator(creator);
          this.setState({ loading: false });
        })
        .catch(err => {
          Toast.show(err.message);
          window.localStorage.removeItem('token');
          this.setState({ loading: false });
        });
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    if (this.state.loading) return null;
    return this.props.children;
  }
}

export default connect(
  null,
  { setCreator }
)(AuthWrapper);
