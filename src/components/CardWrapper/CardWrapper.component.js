import React from 'react';
import { Paper } from '@material-ui/core';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const StyledPaper = styled(Paper)`
  padding: 30px;
  margin: 15vh auto 15vh auto;
`;

export default function(props) {
  return (
    <StyledPaper style={{ maxWidth: '400px' }}>
      <Link to="/login" />

      {props.children}
    </StyledPaper>
  );
}
