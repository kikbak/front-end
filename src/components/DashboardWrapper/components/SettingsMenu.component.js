import React from 'react';
import {
  Menu,
  MenuItem,
  Dialog,
  DialogContent,
  Button,
  DialogActions,
  DialogContentText
} from '@material-ui/core';
import { connect } from 'react-redux';
import { clearCreator } from 'ducks/creator.duck';
import { Link } from 'react-router-dom';

class SettingsMenu extends React.Component {
  state = {
    signOutOpen: false
  };

  toggleSignOut = () => {
    this.setState({ signOutOpen: !this.state.signOutOpen });
    this.props.onClose();
  };

  render() {
    return (
      <React.Fragment>
        <Dialog open={this.state.signOutOpen} onClose={this.toggleSignOut}>
          <DialogContent>
            <DialogContentText>
              Are you sure you want to sign out?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.toggleSignOut} color="default">
              Cancel
            </Button>
            <Button onClick={this.props.clearCreator} color="primary" autoFocus>
              Sign out
            </Button>
          </DialogActions>
        </Dialog>

        <Menu
          anchorEl={this.props.anchor}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={Boolean(this.props.anchor)}
          onClose={this.props.onClose}
        >
          <MenuItem
            component={Link}
            to={'/settings/' + this.props.creator.id}
            onClick={this.props.onClose}
            color="inherit"
          >
            Edit information
          </MenuItem>
          <MenuItem>Change password</MenuItem>
          <MenuItem onClick={this.toggleSignOut}>Sign out</MenuItem>
        </Menu>
      </React.Fragment>
    );
  }
}

export default connect(
  null,
  { clearCreator }
)(SettingsMenu);
