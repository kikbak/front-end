import React from 'react';
import { Menu, MenuItem } from '@material-ui/core';

class MessagesMenu extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Menu
          anchorEl={this.props.anchor}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={Boolean(this.props.anchor)}
          onClose={this.props.onClose}
        >
          <MenuItem>Message demo</MenuItem>
          <MenuItem>Message demo</MenuItem>
        </Menu>
      </React.Fragment>
    );
  }
}

export default MessagesMenu;
