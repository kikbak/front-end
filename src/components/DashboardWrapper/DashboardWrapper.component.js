import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Toolbar, AppBar, IconButton, Icon } from '@material-ui/core';
import styled from 'styled-components';
import theme from 'helpers/theme.helper.js';
import AccountMenuIcon from '@material-ui/core/Avatar';
import SettingsMenuIcon from '@material-ui/icons/MoreVert';
import SettingsMenu from './components/SettingsMenu.component';
import NotificationsMenu from './components/NotificationsMenu.component';
import MessagesMenu from './components/MessagesMenu.component';

const styles = {
  flex: { display: 'flex' },
  grow: { flexGrow: 1 },
  appBar: { zIndex: theme.zIndex.drawer + 1, backgroundColor: 'white' },
  content: { width: '100%' },
  font: { fontFamily: 'roboto', fontSize: '1em' },
  avatar: { width: 35, height: 35 },
  appBarContent: {
    flexGrow: 1,
    display: 'flex',
    padding: 0,
    justifyContent: 'left'
  }
};

const Logo = styled.img`
  width: 30px;
  max-width: 100%;
  display: block;
`;

class DashboardWrapper extends React.Component {
  state = {
    settingsMenuAnchor: null,
    notificationsMenuAnchor: null,
    messagesMenuAnchor: null
  };

  render() {
    const creator = this.props.creator;

    return (
      <React.Fragment>
        <AppBar position="fixed" style={styles.appBar}>
          <Toolbar>
            <div style={styles.appBarContent}>
              <Icon component={Link} to="/home">
                <Logo
                  src="https://s3-us-west-2.amazonaws.com/name-tba/logo.png"
                  alt="aqva-logo"
                />
              </Icon>
            </div>

            <IconButton component={Link} to="/map" color="inherit">
              <svg
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"
                  stroke="black"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M2 12H22"
                  stroke="black"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M12 2C14.5013 4.73835 15.9228 8.29203 16 12C15.9228 15.708 14.5013 19.2616 12 22C9.49872 19.2616 8.07725 15.708 8 12C8.07725 8.29203 9.49872 4.73835 12 2V2Z"
                  stroke="black"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </IconButton>

            <NotificationsMenu
              anchor={this.state.notificationsMenuAnchor}
              onClose={() => this.setState({ notificationsMenuAnchor: null })}
            />

            <MessagesMenu
              anchor={this.state.messagesMenuAnchor}
              onClose={() => this.setState({ messagesMenuAnchor: null })}
            />

            <IconButton
              onClick={e =>
                this.setState({ messagesMenuAnchor: e.currentTarget })
              }
              color="inherit"
            >
              <svg
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M22 12H16L14 15H10L8 12H2"
                  stroke="black"
                  strokeOpacity="0.87"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M5.45 5.11L2 12V18C2 18.5304 2.21071 19.0391 2.58579 19.4142C2.96086 19.7893 3.46957 20 4 20H20C20.5304 20 21.0391 19.7893 21.4142 19.4142C21.7893 19.0391 22 18.5304 22 18V12L18.55 5.11C18.3844 4.77679 18.1292 4.49637 17.813 4.30028C17.4967 4.10419 17.1321 4.0002 16.76 4H7.24C6.86792 4.0002 6.50326 4.10419 6.18704 4.30028C5.87083 4.49637 5.61558 4.77679 5.45 5.11V5.11Z"
                  stroke="black"
                  strokeOpacity="0.87"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </IconButton>

            <IconButton
              onClick={e =>
                this.setState({ notificationsMenuAnchor: e.currentTarget })
              }
              color="inherit"
            >
              <svg
                width="20"
                height="20"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 2L15.09 8.26L22 9.27L17 14.14L18.18 21.02L12 17.77L5.82 21.02L7 14.14L2 9.27L8.91 8.26L12 2Z"
                  stroke="black"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </IconButton>

            <IconButton
              component={Link}
              to="/portfolio/me"
              onClick={this.props.onClose}
              color="inherit"
            >
              <AccountMenuIcon
                alt={creator.username}
                src={creator.aviUrl}
                style={styles.avatar}
              />
            </IconButton>

            <SettingsMenu
              anchor={this.state.settingsMenuAnchor}
              creator={this.props.creator}
              onClose={() => this.setState({ settingsMenuAnchor: null })}
            />

            <IconButton
              onClick={e =>
                this.setState({ settingsMenuAnchor: e.currentTarget })
              }
              color="inherit"
            >
              <SettingsMenuIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Toolbar />
        <div style={styles.flex}>
          <div style={styles.content}>{this.props.children}</div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(state => ({ creator: state.creator }))(DashboardWrapper);
