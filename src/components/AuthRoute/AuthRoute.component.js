import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

export default connect(state => ({
  creator: state.creator
}))(props => {
  window.scrollTo(0, 0);

  // No redirect needed.
  if ((props.secure && props.creator) || (!props.secure && !props.creator)) {
    return <Route {...props} />;
  }

  // Not signed in. Redirect to login.
  if (!props.creator) {
    // Store original path.
    window.sessionStorage.setItem('original_path', window.location.pathname);
    return <Redirect to="/" />;
  }

  // Signed in. Redirect to home or originally requested route.
  const path = window.sessionStorage.getItem('original_path') || '/home';
  window.sessionStorage.removeItem('original_path');
  return <Redirect to={path} />;
});
