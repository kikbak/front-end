import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './ducks';
import theme from 'helpers/theme.helper';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Toast, AuthWrapper } from 'components';
import { UniversalModal } from './components';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <Toast />
          <UniversalModal />
          <AuthWrapper>
            <BrowserRouter>
              <Routes />
            </BrowserRouter>
          </AuthWrapper>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
