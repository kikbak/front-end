import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { AuthRoute } from 'components';
import Login from './Login/Login.route';
import Signup from './Signup/Signup.route';
import Home from './Home/Home.route';
import Map from './Map/Map.route';
import Portfolio from './Portfolio/Portfolio.route';
import Settings from './Settings/Settings.route';
import CatchAll from './CatchAll/CatchAll.route';

export default () => {
  return (
    <Switch>
      <AuthRoute path="/" exact component={Login} />
      <AuthRoute path="/sign-up" exact component={Signup} />
      <AuthRoute path="/home" secure exact component={Home} />
      <AuthRoute path="/map" secure exact component={Map} />
      <AuthRoute path="/portfolio/:id" secure exact component={Portfolio} />
      <AuthRoute path="/settings/:id" secure exact component={Settings} />
      <Route component={CatchAll} />
    </Switch>
  );
};
