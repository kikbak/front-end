import React from 'react';
import { connect } from 'react-redux';
import { Typography, Grid, Paper } from '@material-ui/core';
import { DashboardWrapper, Toast } from 'components';
import ProfileTabs from './components/VerticalTab.component';
import http from 'helpers/http.helper.js';

class Settings extends React.Component {
  state = {
    creator: null
  };

  componentDidMount() {
    this.getCreator();
  }

  getCreator = () => {
    http()
      .get('/creators/' + this.props.creator.id)
      .then(res => this.setState({ creator: res }))
      .catch(err => Toast.show(err.message));
  };

  openModal = (modalName, options = {}) => {
    const isCreator = this.props.creator;
    if (options.adminOnly && !isCreator) {
      return undefined;
    } else if (isCreator) {
      return () => this.setState({ [modalName]: true });
    }
  };

  render() {
    const { creator } = this.state;

    if (!creator) return <DashboardWrapper />;
    return (
      <DashboardWrapper>
        <Typography variant="h6" style={{ textAlign: 'center' }} gutterBottom>
          Settings
        </Typography>
        <Grid container>
          <Grid item xs={12}>
            <Paper elevation={1}>
              <ProfileTabs />
            </Paper>
          </Grid>
        </Grid>
      </DashboardWrapper>
    );
  }
}

export default connect(state => ({
  creator: state.creator
}))(Settings);
