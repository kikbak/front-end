import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Divider
} from '@material-ui/core';
import { connect } from 'react-redux';
import { setCreator } from 'ducks/creator.duck';
import http from 'helpers/http.helper';
import { Toast } from 'components';

// Cropper
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

class ChangeAviModal extends React.Component {
  state = {
    file: null,
    previewPhoto: ''
  };

  onChange = e => {
    this.setState({
      file: e.target.files[0],
      previewPhoto: URL.createObjectURL(e.target.files[0])
    });
  };

  _crop = () => {
    var base64 = this.refs.cropper.getCroppedCanvas().toDataURL();
    var arrayOfData = base64.split(',');
    var mime = arrayOfData[0].match(/:(.*?);/);
    var bstr = atob(arrayOfData[1]);
    var size = bstr.length;
    var uArray = new Uint8Array(size);

    while (size--) {
      uArray[size] = bstr.charCodeAt(size);
    }

    const file = new File([uArray], `${this.props.creator.username}-avatar`, {
      type: mime
    });

    const form = new FormData();
    form.append('file', file);
    http()
      .post('/img-uploads', form)
      .then(res => {
        http().put(`/creators/${this.props.creator.id}`, {
          aviUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/' + res.fileName
        });
      })
      .then(() => {
        this.props.refresh();
        this.props.onClose();
      })
      .catch(err => Toast.show(err.message));
  };

  render() {
    const { file, previewPhoto } = this.state;

    return (
      <Dialog
        maxWidth="sm"
        fullWidth
        open={this.props.open}
        onClose={this.props.onClose}
        onEnter={this.setProject}
        style={{ textAlign: 'center', padding: 15 }}
      >
        <form autoComplete="off">
          <DialogTitle>Change Photo</DialogTitle>
          <DialogContent>
            {file ? (
              <div style={{ position: 'relative' }}>
                <Cropper
                  autoCropArea={1}
                  ref="cropper"
                  aspectRatio={1}
                  style={{ height: 250, width: 250, margin: '0 auto' }}
                  src={previewPhoto}
                  guides={false}
                  center={false}
                />
                <DialogActions>
                  <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={this._crop}
                  >
                    save
                  </Button>
                </DialogActions>
              </div>
            ) : (
              <React.Fragment>
                <input
                  accept="image/*"
                  id="bt-img-upload"
                  style={{ display: 'none' }}
                  type="file"
                  onChange={this.onChange}
                />
                <label htmlFor="bt-img-upload">
                  <Button
                    fullWidth
                    variant="text"
                    color="primary"
                    component="span"
                  >
                    Upload from computer
                  </Button>
                </label>
                <Button fullWidth style={{ color: '#FF0000', marginBottom: 8 }}>
                  Remove Current Photo
                </Button>
              </React.Fragment>
            )}

            <Divider />
          </DialogContent>

          <DialogActions>
            <Button fullWidth onClick={this.props.onClose}>
              Cancel
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    );
  }
}

export default connect(
  null,
  { setCreator }
)(ChangeAviModal);
