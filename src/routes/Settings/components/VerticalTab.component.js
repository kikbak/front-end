import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core';
import EditPortfolioTab from './EditPortfolioTab.component';
import ChangePasswordTab from './ChangePasswordTab.component';

const tabButtons = ['Edit Portfolio', 'Change Password'];

const MyTab = withStyles(theme => ({
  selected: {
    color: '#F5BF1E',
    borderRight: '2px solid #F5BF1E'
  }
}))(Tab);

const VerticalTabs = withStyles(theme => ({
  flexContainer: {
    flexDirection: 'column'
  },
  indicator: {
    display: 'none'
  }
}))(Tabs);

class ProfileTabs extends React.PureComponent {
  state = { activeTab: 0 };

  handleChange = (_, activeTab) => this.setState({ activeTab });
  render() {
    const { activeTab } = this.state;

    return (
      <div
        style={{
          display: 'flex'
        }}
      >
        <VerticalTabs value={activeTab} onChange={this.handleChange}>
          {tabButtons.map(title => (
            <MyTab key={title} label={title} />
          ))}
        </VerticalTabs>

        {activeTab === 0 && <EditPortfolioTab />}
        {activeTab === 1 && <ChangePasswordTab />}
      </div>
    );
  }
}

export default ProfileTabs;
