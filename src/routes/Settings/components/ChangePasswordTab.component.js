import React from 'react';
import { TextField, Button, Grid, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import styled from 'styled-components';

const Avi = styled.img`
  width: 100px;
  height: 100px;
  display: block;
  border-radius: 50%;
  object-fit: cover;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  cursor: pointer;
`;

class ChangePasswordTab extends React.Component {
  state = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: ''
  };

  render() {
    return (
      <div style={{ padding: 50, flexGrow: 1 }}>
        <Grid container spacing={16} style={{ margin: 'auto' }}>
          <Grid item>
            <Avi src={this.props.creator.aviUrl} />
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs>
              <div style={{ marginLeft: 10 }}>
                <Typography gutterBottom component="h4" variant="h6">
                  {'@' + this.props.creator.username}
                </Typography>
              </div>
            </Grid>
          </Grid>
        </Grid>

        <form>
          <TextField
            name="oldPassword"
            label="old password"
            type="password"
            required
            fullWidth
            margin="normal"
            value={this.state.oldPassword}
            onChange={this.handleChange}
          />
          <TextField
            name="password"
            label="password"
            type="password"
            required
            fullWidth
            margin="normal"
            value={this.state.password}
            onChange={this.handleChange}
          />
          <TextField
            name="confirmPassword"
            label="confirm password"
            type="password"
            required
            fullWidth
            margin="normal"
            value={this.state.confirmPassword}
            onChange={this.handleChange}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            style={{
              margin: '20px 0 5px',
              color: 'white',
              borderRadius: '15px'
            }}
          >
            Change Password
          </Button>
        </form>
      </div>
    );
  }
}

export default connect(state => ({
  creator: state.creator
}))(ChangePasswordTab);
