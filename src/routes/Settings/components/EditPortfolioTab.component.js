import React from 'react';
import { connect } from 'react-redux';
import ChangeAviModal from '../modals/ChangeAvi.modal';
import { Grid, Button, TextField, Typography } from '@material-ui/core';
import styled from 'styled-components';
import http from 'helpers/http.helper';
import { Toast } from 'components';

const Avi = styled.img`
  width: 100px;
  height: 100px;
  display: block;
  border-radius: 50%;
  object-fit: cover;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  cursor: pointer;
`;

class EditPortfolioTab extends React.Component {
  state = {
    creator: null,
    username: this.props.creator.username,
    firstName: this.props.creator.firstName,
    lastName: this.props.creator.lastName,
    email: this.props.creator.email,
    changeAviModal: false
  };

  componentDidMount() {
    this.getCreator();
  }

  getCreator = () => {
    const creatorId = this.props.creator.id;

    http()
      .get('/creators/' + creatorId)
      .then(res => this.setState({ creator: res }))
      .catch(err => Toast.show(err.message));
  };

  save = e => {
    e.preventDefault();

    http()
      .put('/creators/' + this.props.creator.id, {
        username: this.state.username,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email
      })
      .then(() => {
        Toast.show(`${this.state.username} was updated.`);
        this.getCreator();
      })
      .catch(err => Toast.show(err.message));
  };

  archive = () => {
    http()
      .delete('/creators/' + this.props.creator.id)
      .then(() => {
        Toast.show("We're sad to see you go.");
      })
      .catch(err => Toast.show(err.message));
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { username, firstName, lastName, email } = this.state;
    if (!this.state.creator) return <div />;
    return (
      <div style={{ padding: 50, flexGrow: 1 }}>
        <ChangeAviModal
          open={this.state.changeAviModal}
          creator={this.props.creator}
          refresh={this.getCreator}
          onClose={() => this.setState({ changeAviModal: false })}
        />
        <Grid container spacing={16} style={{ marginBottom: 40 }}>
          <Grid item>
            <Avi
              src={this.state.creator.aviUrl}
              onClick={() => this.setState({ changeAviModal: true })}
            />
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={16}>
              <Grid item xs>
                <div style={{ marginLeft: 10 }}>
                  {this.state.creator && (
                    <Typography gutterBottom component="h4" variant="h6">
                      {'@' + this.state.creator.username}
                    </Typography>
                  )}
                </div>
                <Button
                  color="primary"
                  onClick={() => this.setState({ changeAviModal: true })}
                >
                  Upload new Photo
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <form onSubmit={this.save}>
          <TextField
            fullWidth
            label="Username"
            name="username"
            onChange={this.handleChange}
            value={username}
          />
          <TextField
            fullWidth
            label="First Name"
            name="firstName"
            onChange={this.handleChange}
            value={firstName}
          />
          <TextField
            fullWidth
            label="Last Name"
            name="lastName"
            onChange={this.handleChange}
            value={lastName}
          />

          <Typography
            variant="subtitle1"
            gutterBottom
            style={{ marginTop: 30, marginBottom: 30 }}
          >
            Private Information
          </Typography>
          <TextField
            fullWidth
            onChange={this.handleChange}
            label="Email"
            name="email"
            value={email}
          />
          <Grid container spacing={24} style={{ marginTop: 20 }}>
            <Grid item xs={6}>
              <Button color="primary" type="submit">
                Save Changes
              </Button>
            </Grid>
            <Grid item xs={6} style={{ textAlign: 'right' }}>
              <Button style={{ color: '#FF0000' }}>Disable My Account</Button>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}

export default connect(state => ({
  creator: state.creator
}))(EditPortfolioTab);
