import React from 'react';
import GoogleMapReact from 'google-map-react';
import { connect } from 'react-redux';
import { DashboardWrapper } from 'components';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Map extends React.Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };

  render() {
    return (
      <DashboardWrapper>
        <div style={{ height: '85vh', width: '100%' }}>
          <GoogleMapReact
            defaultCenter={this.props.center}
            defaultZoom={this.props.zoom}
          >
            <AnyReactComponent
              lat={59.955413}
              lng={30.337844}
              text="My Marker"
            />
          </GoogleMapReact>
        </div>
      </DashboardWrapper>
    );
  }
}

export default connect(state => ({ creator: state.creator }))(Map);
