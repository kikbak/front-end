import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { setCreator } from 'ducks/creator.duck';
import { Toast, CardWrapper } from 'components';
import styled from 'styled-components';
import { Button, TextField, Typography, Checkbox } from '@material-ui/core';
import http from 'helpers/http.helper.js';

const Logo = styled.img`
  margin: auto;
  max-width: 100%;
  display: block;
`;

const styles = {
  wrapper: {
    display: 'flex',
    padding: 0,
    justifyContent: 'center'
  }
};
class Login extends React.Component {
  state = {
    // TODO: add Google and Facebook Auth
    email: '',
    password: '',
    stayLoggedIn: false
  };

  signIn = e => {
    e.preventDefault();
    http()
      .post('/creators/login', {
        email: this.state.email,
        password: this.state.password
      })
      .then(creator => {
        Toast.show('Welcome: ' + creator.name);
        this.props.setCreator(creator);
      })
      .catch(err => Toast.show(err.message));
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChecked = e => {
    this.setState({ [e.target.name]: e.target.checked });
  };

  render() {
    return (
      <CardWrapper>
        <div style={styles.wrapper}>
          <Logo
            src="https://s3-us-west-2.amazonaws.com/name-tba/logo.png"
            alt="aqva-logo"
            style={{ marginLeft: '0', marginRight: '10px' }}
          />
          <div>
            <Typography component="h1" variant="h4">
              {' '}
              Login
            </Typography>
          </div>
        </div>

        <form style={{ padding: 25 }} onSubmit={this.signIn}>
          <TextField
            name="email"
            label="Email Address"
            autoFocus
            required
            fullWidth
            margin="normal"
            value={this.state.email}
            onChange={this.handleChange}
          />
          <TextField
            name="password"
            label="Password"
            autoComplete="current-password"
            type="password"
            required
            fullWidth
            margin="normal"
            value={this.state.password}
            onChange={this.handleChange}
          />
          <div style={styles.wrapper}>
            <Checkbox
              color="primary"
              checked={this.state.stayLoggedIn}
              onChange={this.handleChecked}
              name="stayLoggedIn"
            />
            <Typography
              style={{
                margin: 'auto 0px'
              }}
            >
              Stay logged in
            </Typography>

            <div
              style={{
                margin: 'auto'
              }}
            >
              <Typography
                component={Link}
                to="/forgot-password"
                size="small"
                color="primary"
                style={{
                  textDecoration: 'none'
                }}
              >
                Forgot Password
              </Typography>
            </div>
          </div>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            style={{
              margin: '10px 0 5px',
              color: 'white',
              borderRadius: '15px'
            }}
          >
            Sign in
          </Button>
        </form>

        <div style={styles.wrapper}>
          <Typography>Don't have an account? </Typography>
          <div>
            <Typography
              component={Link}
              to="/sign-up"
              size="small"
              color="primary"
              style={{
                marginLeft: '3px',
                textDecoration: 'none'
              }}
            >
              Sign Up
            </Typography>
          </div>
        </div>
      </CardWrapper>
    );
  }
}

export default connect(
  null,
  { setCreator }
)(Login);
