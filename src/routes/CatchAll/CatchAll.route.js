import React from 'react';
import { Typography } from '@material-ui/core';

export default class CatchAll extends React.Component {
  render() {
    return <Typography>404 Not Found</Typography>;
  }
}
