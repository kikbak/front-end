import React from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  gridContainer: {
    margin: '0 auto',
    textAlign: 'center',
    backgroundColor: 'white',
    border: '1px solid #D4D4D4',
    borderRadius: '3px'
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  }
};

const Avi = styled.img`
  width: 150px;
  height: 150px;
  display: block;
  object-fit: cover;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
`;

class CreatorInfo extends React.Component {
  render() {
    const { creator, creatorIsSelf } = this.props;

    return (
      <Grid container spacing={24} style={styles.gridContainer}>
        <CreatorAvi aviUrl={creator.aviUrl} />
        <Grid item xs={12}>
          <Typography variant="h4" gutterBottom>
            {creator.name}
          </Typography>
          <Typography variant="h6" color="textSecondary">
            Idaho Falls, ID
          </Typography>
        </Grid>

        <CreatorButtons creatorIsSelf={creatorIsSelf} />

        <Grid item xs={12}>
          <Typography
            variant="subtitle1"
            color="textSecondary"
            style={{ textAlign: 'left' }}
          >
            Labels
          </Typography>
          {/* TODO: New component for the chips that displays the creator's labels */}
        </Grid>

        <Grid item xs={12}>
          <Typography
            variant="subtitle1"
            color="textSecondary"
            style={{ textAlign: 'left' }}
          >
            Reviews
          </Typography>
          {/* TODO: New component for the chips that displays the creator's reviews */}
        </Grid>
      </Grid>
    );
  }
}

function CreatorButtons(props) {
  const { creatorIsSelf } = props;
  return (
    <Grid item xs={12} style={styles.buttonContainer}>
      {creatorIsSelf ? (
        <Button variant="contained" color="primary">
          Edit Portfolio
        </Button>
      ) : (
        <React.Fragment>
          <Button
            variant="contained"
            color="secondary"
            style={{ marginRight: 20 }}
          >
            Hire me
          </Button>
          <Button
            variant="contained"
            color="secondary"
            style={{ marginRight: 20 }}
          >
            Message
          </Button>
        </React.Fragment>
      )}
    </Grid>
  );
}

function CreatorAvi(props) {
  const { aviUrl } = props;

  return (
    <Grid item xs={12}>
      <Avi src={aviUrl} />
    </Grid>
  );
}

export default withStyles(styles)(CreatorInfo);
