import React from 'react';
import { connect } from 'react-redux';
import { Toast } from 'components';
import { GridList, GridListTile } from '@material-ui/core';
import http from 'helpers/http.helper';
import styled from 'styled-components';

// const Overlay = styled.div`
//   position: absolute;
//   top: 0;
//   left: 0;
//   right: 0;
//   bottom: 0;
//   display: flex;
//   align-items: center;
//   justifycontent: center;
//   z-index: 1;
//   opacity: 0;
//   &:hover {
//     background-color: rgba(0, 0, 0, 0.6);
//     opacity: 1;
//   }
// `;

const Photo = styled.img`
  margin: auto;
  max-width: 100%;
  max-height: 100%;
  width: auto;
  height: auto;
  display: block;
`;

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden'
  },
  gridList: {
    height: 600,
    marginTop: 16
  },
  buttonContainer: {
    bottom: '10px',
    right: '20px',
    position: 'absolute',
    justifyContent: 'flex-end',
    display: 'flex',
    color: 'white',
    fontFamily: 'Roboto',
    opacity: 1
  }
};

class CreatorContent extends React.Component {
  state = {
    value: 0,
    portfolioPosts: []
  };

  componentDidMount() {
    this.getPortfolioPosts();
  }

  getPortfolioPosts = () => {
    http()
      .get(`/portfolio-posts?createdBy=${this.props.creator.id}`)
      .then(res => {
        this.setState({ portfolioPosts: res });
      })
      .catch(err => Toast.show(err.message));
  };

  postLiked = postId => {
    http()
      .post(`/portfolio-posts/${postId}/favorites`)
      .then(() => this.getPortfolioPosts())
      .catch(err => Toast.show(err.message));
  };

  render() {
    const { width } = this.props;
    const { portfolioPosts } = this.state;

    let columns = width === 'xs' || width === 'sm' ? 1 : 2;
    console.log(portfolioPosts);
    return (
      <GridList cellHeight={350} style={styles.gridList} cols={columns}>
        {portfolioPosts.map(post => (
          <GridListTile key={post.id} cols={1}>
            {/* <Overlay>
              <div style={styles.buttonContainer}>
                <p>{post.favoriteCount ? post.favoriteCount : 0}</p>
                <svg
                  onClick={() => this.postLiked(post.id)}
                  style={{ margin: 'auto', marginLeft: 5 }}
                  width="20"
                  height="20"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12 2L15.09 8.26L22 9.27L17 14.14L18.18 21.02L12 17.77L5.82 21.02L7 14.14L2 9.27L8.91 8.26L12 2Z"
                    stroke="white"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </Overlay> */}
            <Photo src={post.imgUrl} alt={post.id} />
          </GridListTile>
        ))}
      </GridList>
    );
  }
}

export default connect(state => ({ creator: state.creator }))(CreatorContent);
