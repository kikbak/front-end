import React from 'react';
import { connect } from 'react-redux';
import { Grid, IconButton } from '@material-ui/core';
import {
  DashboardWrapper,
  Toast,
  PortfolioUploadIcon,
  MoreHorizontal
} from 'components';
import http from 'helpers/http.helper.js';
import CreatorInfoComponent from './components/CreatorInfo.component';
import CreatorContent from './components/CreatorContent.component';
import { showModal } from 'ducks/modal.duck';

const styles = {
  contentContainer: {
    margin: 'auto'
  },
  contentMenuBar: {
    flexGrow: 1,
    display: 'flex',
    padding: 0,
    margin: 0,
    justifyContent: 'flex-end'
  }
};

class Portfolio extends React.Component {
  state = {
    creator: null,
    portfolioPosts: []
  };

  componentDidMount() {
    this.getCreator();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.getCreator();
    }
  }

  getCreator = () => {
    const creatorId = this.props.match.params.id;

    http()
      .get('/creators/' + creatorId)
      .then(res => this.setState({ creator: res }))
      .catch(err => Toast.show(err.message));
  };

  render() {
    const { creator } = this.state;

    if (!creator) return <DashboardWrapper />;

    const creatorIsSelf = creator.id === this.props.creator.id;
    return (
      <DashboardWrapper>
        <Grid container spacing={8}>
          <Grid item xs={11} sm={5} md={3}>
            <CreatorInfoComponent
              creator={creator}
              creatorIsSelf={creatorIsSelf}
            />
          </Grid>
          <Grid item xs={11} sm={6} md={8} style={styles.contentContainer}>
            <Grid item xs={12}>
              <div style={styles.contentMenuBar}>
                <div>
                  <IconButton
                    onClick={() =>
                      this.props.showModal({
                        modalType: 'portfolioUpload',
                        modalProps: {
                          creator: creator,
                          refresh: this.getCreator
                        }
                      })
                    }
                  >
                    <PortfolioUploadIcon />
                  </IconButton>

                  <IconButton>
                    <MoreHorizontal />
                  </IconButton>
                </div>
              </div>
            </Grid>
            <CreatorContent creatorIsSelf={creatorIsSelf} />
          </Grid>
        </Grid>
      </DashboardWrapper>
    );
  }
}

export default connect(
  state => ({ creator: state.creator }),
  { showModal }
)(Portfolio);
