import React from 'react';
import { connect } from 'react-redux';
import { Typography, GridList, GridListTile } from '@material-ui/core';
import { DashboardWrapper, Toast } from 'components';
import CreatorCard from './components/CreatorCard.component';
import http from 'helpers/http.helper.js';

class Home extends React.Component {
  state = {
    creators: []
  };

  componentDidMount() {
    this.getCreators();
  }

  getCreators = () => {
    http()
      .get('/creators')
      .then(res => {
        this.setState({
          creators: res
        });
      })
      .catch(err => Toast.show(err.message));
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <DashboardWrapper>
        <Typography variant="h6" gutterBottom>
          Photographers like you
        </Typography>
        <GridList
          style={{ flexWrap: 'nowrap', transform: 'translateZ(0)' }}
          cols={2.5}
        >
          {this.state.creators.map(row => (
            <GridListTile>
              <CreatorCard
                user={row}
                key={row.id}
                aviUrl={row.aviUrl}
                username={row.username}
                rowAction={row =>
                  this.props.history.push(`/portfolio/${row.id}`)
                }
              />
            </GridListTile>
          ))}
        </GridList>
      </DashboardWrapper>
    );
  }
}

export default connect(state => ({ creator: state.creator }))(Home);
