import React from 'react';
import { Paper, Typography, Button } from '@material-ui/core';
import styled from 'styled-components';

const CreatorAvi = styled.img`
  width: 150px;
  height: 150px;
  display: block;
  object-fit: cover;
  margin: 0 auto;
  margin-bottom: 15px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
`;

class CreatorCard extends React.Component {
  render() {
    const { aviUrl, username, user, rowAction } = this.props;
    return (
      <React.Fragment>
        <Paper style={{ padding: 15 }}>
          <CreatorAvi src={aviUrl} />
          <Typography variant="subtitle2">{username}</Typography>
          <Typography variant="caption">Rating: 5.0</Typography>
          <Button
            fullWidth
            onClick={() => rowAction(user)}
            variant="contained"
            color="primary"
            style={{
              margin: '10px 0 5px',
              color: 'white',
              borderRadius: '15px'
            }}
          >
            View Portfolio
          </Button>
        </Paper>
      </React.Fragment>
    );
  }
}

export default CreatorCard;
