import React from 'react';
import { connect } from 'react-redux';
import { setCreator } from 'ducks/creator.duck';
import { Typography, Button, TextField } from '@material-ui/core';
import styled from 'styled-components';
import http from 'helpers/http.helper.js';
import { Toast, CardWrapper } from 'components';

const Logo = styled.img`
  margin: auto;
  max-width: 100%;
  display: block;
`;

class Signup extends React.Component {
  state = {
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    password: '',
    confirmPassword: ''
  };

  createAccount = e => {
    e.preventDefault();

    http()
      .post('/creators/', {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
        isVerified: false
      })
      .then(creator => {
        Toast.show('your create was created.');
        this.props.setCreator(creator);
      })
      .catch(err => Toast.show(err.message));
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <CardWrapper>
        <div
          style={{
            display: 'flex',
            padding: 0,
            justifyContent: 'center'
          }}
        >
          <Logo
            src="https://s3-us-west-2.amazonaws.com/name-tba/logo.png"
            alt="aqva-logo"
            style={{ marginLeft: '0', marginRight: '10px' }}
          />
          <div>
            <Typography component="h1" variant="h4">
              {' '}
              Sign Up
            </Typography>
          </div>
        </div>
        <form style={{ padding: 25 }} onSubmit={this.createAccount}>
          <TextField
            name="username"
            label="enter username"
            required
            fullWidth
            margin="normal"
            value={this.state.username}
            onChange={this.handleChange}
          />

          <TextField
            name="email"
            label="enter email"
            required
            fullWidth
            margin="normal"
            value={this.state.email}
            onChange={this.handleChange}
          />

          <TextField
            name="firstName"
            label="First Name"
            required
            fullWidth
            margin="normal"
            value={this.state.FirstName}
            onChange={this.handleChange}
          />

          <TextField
            name="lastName"
            label="Last Name"
            required
            fullWidth
            margin="normal"
            value={this.state.lastName}
            onChange={this.handleChange}
          />
          <TextField
            name="password"
            label="password"
            type="password"
            required
            fullWidth
            margin="normal"
            value={this.state.password}
            onChange={this.handleChange}
          />
          <TextField
            name="confirmPassword"
            label="confirm password"
            type="password"
            required
            fullWidth
            margin="normal"
            value={this.state.confirmPassword}
            onChange={this.handleChange}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            style={{
              margin: '20px 0 5px',
              color: 'white',
              borderRadius: '15px'
            }}
          >
            Create Account
          </Button>
        </form>

        <Button
          fullWidth
          onClick={() => this.props.history.push('/')}
          size="small"
          color="primary"
          style={{ margin: '8px 0 15px' }}
        >
          Back to Login
        </Button>
      </CardWrapper>
    );
  }
}
export default connect(
  null,
  { setCreator }
)(Signup);
