import { createMuiTheme } from '@material-ui/core/styles';

export const colors = {
  primary: '#F5BF1E',
  secondary: '#b9b9b9',
  background: '#E5E5E5',
  grey: '#e0e0e0',
  success: '#1b7730',
  red: 'FF0000'
};

export const font = {
  fontFamily: 'roboto'
};

export default createMuiTheme({
  palette: {
    primary: { main: colors.primary },
    secondary: { main: colors.secondary },
    background: { default: colors.background },
    red: { red: colors.red }
  },
  typography: {
    useNextVariants: true
  },
  overrides: {
    MuiSelect: {
      select: {
        '&:focus': {
          background: 'none'
        }
      }
    }
  }
});
