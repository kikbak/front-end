import axios from 'axios';
import pubsub from 'helpers/pubsub.helper.js';

export default ({ withLoader = true } = {}) => {
  if (withLoader) {
    pubsub.publish('LOADING_START');
  }

  const instance = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
      Authorization: window.localStorage.getItem('token')
    }
  });

  instance.interceptors.response.use(
    response => {
      if (withLoader) {
        pubsub.publish('LOADING_END');
      }

      return response.data;
    },
    error => {
      if (withLoader) {
        pubsub.publish('LOADING_END');
      }

      return error.response &&
        error.response.data &&
        error.response.data.message
        ? Promise.reject(error.response.data)
        : Promise.reject({
            message:
              'Oops! Looks like a server error. Contact support if the problem persists.'
          });
    }
  );

  return instance;
};
